import sys


def add_payment():
    print('\nДобавить платеж\n')


def update_payment():
    print('\nОтредактировать платеж\n')


def print_payments_for_period():
    print('\nВывести все платежи за указанный период\n')


def top_payments():
    print('\nВывести топ самых крупных платежей\n')


def print_menu():
    for key in dict1:
        print(f'{key}. {dict1[key][0]}')


def close_program():
    sys.exit(0)


dict1 = {
    '1': ('Добавить платеж', add_payment),
    '2': ('Отредактировать платеж', update_payment),
    '3': ('Вывести все платежи за указанный период', print_payments_for_period),
    '4': ('Вывести топ самых крупных платежей', top_payments),
    '5': ('Показать меню', print_menu),
    '6': ('Закрыть программу', close_program),
}


while True:
    print_menu()
    try:
        user_input = input('\nВведите команду: ')
        try:
            dict1[user_input][1]()
        except ValueError as err:
            print(err)
    except ValueError as err:
        print(err)