class Node(object):
    """Узел списка"""
    def __init__(self, value=None, next=None):
        self.value = value # данные
        self.next_ = next   # указатель на следующий узел

    def __repr__(self):
        return f'{self.__class__.__name__}({self.value})'


class LinkedList(object):
    """Односвязный список"""

    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0