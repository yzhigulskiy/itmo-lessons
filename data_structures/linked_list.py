"""
Односвязный список

"""

class Node(object):
    """Узел списка"""
    def __init__(self, value=None, next=None):
        self.value = value # данные
        self.next_ = next   # указатель на следующий узел
    def __repr__(self):
        return f'{self.__class__.__name__}({self.value})'

    
class LinkedList(object):
    """Односвязный список"""
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def _create(self, value):
        """Создает и возвращает новый узел списка с сказанным значением"""
        return Node(value)

    def _prev(self, node):
        """Возвращает предыдущий узел для указанного узла"""
        current = self.head

        if current is node:
            return None

        while current.next is not node:
            current = current.next
        
        return current

    def is_empty(self):
        """Возвращает истинное значение если список пуст, иначе ложь"""
        return self.head is None

    def length(self):
        """Возвращает длинну списка"""
        return self.size

    def nodes(self):
        """Возвращает объект-генератор узлов  списка."""
        current  = self.head
        while current: 
            yield current
            current  = current.next

    def search(self, value):
        """Находит и возвращает первый узел с указанным значением либо None в случае отсутствия."""
        for node in self.nodes():
            if node.value == value:
                return node

    def insert(self, value, node=None):
        """
        Создает и добавляет узел после казанного узла и возвращает добавленный узел. 
        Если узел не задан, добавление нового узла происходит в начало списка.
        """
        current = self._create(value)

        if node is None: # заменяем голову списка
            if self.is_empty():
                self.head = self.tail = current # создаем ссылки на голову и хвост
            else:
                current.next = self.head # иначе текущий узел ссылается на старую голову
                self.head = current      # и сам становится головой
        else:
            if node.next is None: # если вставка в конец списка
                self.tail = current
            else:
                current.next = node.next # текущий узел ссылается на следующий узел указанного в аргумент

            node.next = current # переданный узел ссылается на текущий (новый)

        self.size += 1 # увеличиваем длинну списка на единицу
        return current # возвращает новый узел

    def append(self, value):
        """
        Создает и добавляет в конец списка и возвращает созданный узел.
        """
        return self.insert(value, self.tail)

    def remove(self, node):
        """Удаляет указанный узел из списка."""
        if self.is_empty(): # если список пустой
            return          # то удаляет

        prev = self._prev(node) # получаем предыдущий узел

        if node is self.head: # если удаляем голову
            self.head = self.head.next # то заменяем голову на следующий узел в списке
        else:
            prev.next = node.next # иначе предыдущему узлу меняем ссылку на указатель в next в удаляемом узле

        if node is self.tail: # если удаляем хвост
            self.tail = prev  # то меняем ссылку на хвост

        self.size -= 1 # уменьшаем размер списка на 1 

        return node    # возвращает удаленный элемент

    def remove_head(self):
        """Удаляет голову списка"""
        self.remove(self.head)

    def remove_tail(self):
        """Удаляет хвост списка"""
        self.remove(self.tail)


if __name__ == '__main__':
    l = LinkedList()
    
    node = l.append(1)
    print(node, list(l.nodes()))

    node2 = l.insert(2, node)
    print(node2, list(l.nodes()))

    node3 = l.insert(3)
    print(node3, list(l.nodes()))
