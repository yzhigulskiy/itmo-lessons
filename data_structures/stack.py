from linked_list import LinkedList


class StackError(Exception):
    def __init__(self, stack, *args):
        super().__init__(*args)
        self.stack = stack


class StackOverflow(StackError):
    pass


class StackUnderflow(StackError):
    pass


class Stack(object):
    """Стек на базе односвязного списка."""
    def __init__(self, *values, max_legth=0):
        self.list = LinkedList()
        self.max_length = max_length

        for i in values:
            self.push(i)

        def is_empty(self):
            """Возвращает истину, если стек пустой."""
            return self.list.is_empty()

        def push(self):
            """Добавляет элемент в стек."""
            if 0 < self.max_length <= self.list.length():
                raise StackOverflow(self)
            self.list.insert(value)

        def pop(self):
            """Удаляет и возвращает элемент из стека."""
            if self.is_empty():
                raise StackUnderflow(self)
            return self.list.remove_head().value

        def peek(self):
            """
            Возвращает элемент на вершине стека без удаления его из стека.
            """
            if self.is_empty():
                raise StackUnderflow(self)
            return self.list.head.value


if __name__ == '__main__':
    s = Stack(1, 2, 3)
    print(s.pop())

    s.push(4)
    s.push(5)
    s.push(6)
    print(s.peek())
    print(s.pop())
    print(s.pop())

