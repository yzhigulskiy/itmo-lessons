import push_data
from influxdb import InfluxDBClient
from datetime import datetime as dt


def push_data(result):
    client = InfluxDBClient('localhost', 8086, 'root', 'root', 'example')
    client.create_database('example')
    for i in result.keys():
        if i.isdigit():
            print({"measurement": i, "time": dt.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'), "fields": {"value": result[i]['value']}})
            client.write_points([{"measurement": i, "time": dt.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'), "fields": {"value": result[i]['value']}}])


result = {"request_time": ""}


def data_handler(data):
    """Data handler"""
    global result
    http_code = data.decode('utf-8').split()[13]
    http_request_time = data.decode('utf-8').split()[2][:5]

    if result["request_time"] != http_request_time:
        push_data(result)
        result = {}
        result["request_time"] = http_request_time


    if http_code not in result:
        result[http_code] = {"value": 0}

    result[http_code]["value"] += 1
    #print(result)


'''
json_body = [{"measurement": "http_code", "tags": {"code": "http_code"}, "time": "2018-03-28T8:01:00Z", "fields": {"value": 127}}
'''