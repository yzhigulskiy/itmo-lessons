import socket
import datetime
import data_handler

def log(msg):
    print(
        f'[{datetime.datetime.now():%Y-%m-%d %H:%M:%S}] {msg}'
    )


def server(host='localhost', port=2121, backlog=5):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))

        while 1:
            data = sock.recv(2048)
            if not data:
                break
            data_handler.data_handler(data)


server()