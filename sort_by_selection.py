def get_lowest_element(arr):
    lowest_element = arr[0]
    for temp_element in arr:
        if lowest_element > temp_element:
            lowest_element = temp_element

    return arr.index(lowest_element)


def sort_by_selection(arr):
    sorted_array = []
    for i in (range(len(arr))):
        lowest_index_element = get_lowest_element(arr)
        sorted_array.append(arr[lowest_index_element])
        arr.pop(lowest_index_element)
    return sorted_array


arr = [4, 5, 2, 8, 9, 10, 20, 25, 43, 7, 3, 55, 1, 11]

sorted_arr = sort_by_selection(arr)
print(sorted_arr)