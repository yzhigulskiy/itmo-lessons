# Lesson #1

"""
Multi-line 
comment
"""

"""
todo: How-to init variable into Python?

PEP-8
"""
is_debug = True
name = 'Linus'

print(name)

"""
todo: Data types in Python?
утинная типизация -?

1. Скалярные (простые) типы: bool, int, float, complex, string, str bytes
2. Ссылочные (структурные) типы: tuple, list, set, dict, object
3. Специальный: None

---------------------

Все типы данных в python делятся на:
1. Не изменяемые (immutable): скалярные + tuple
2. Изменяемые (mutable): ссылочные кроме tuple

set - ?
"""

"""
todo: Логичческий тип (bool)
"""
is_student = True
is_admin = False

"""
todo: Целочисленный тип (int)
"""
a = 123
b = -123
c = 0b10101010  # двоичная система исчисления
d = 0o0755 # восьмиричная
e = 0xAF2 # шестнадцатиричная

"""
todo: Числа с плавающей точкой (float)
"""
f = 12.3
f1 = -12.3
f2 = 1e6 # экспонента 1 * 10 ^ 6 => 1000000.0
f3 = 12e-3 # 12 * 10 ^ -3 => 0.0.12

"""
todo: Комплексные числа (complex)
a + bi 
i - мнимя единица
i^2 = -1
"""

с1 = complex(2, 3) # => 2+3j
#c1.real # => a
#c1.imag # => b
#c1.conjugate() # => сопряженное комплексное число

c2 = 2+3j
c3 = 3.14j # => 0+3.14j

"""
TODO: Строки 
"""

s1 = 'Hello'
s2 = "Python"
s3 = '""\''
s4 = '''String'''
s5 = """String"""

s6 = r'' # сырая строка
s7 = u'' # Python2

"""
todo: Байтовая строка (bytes) (Python3)
"""

b1 = b'Hello'
b2 = bytes('Привет', 'utf-8')

"""
todo: Кортеж (tuple)
(1,)
"""

t1 = (1, 2, 3, 1.2, False, '', ("", 3.14j))
print(t1[3], t1[6][1])

"""
todo: Списки (list)
"""

l1 = [1, 2, 3, [], True, ('', '')]
l1[3] = False
print(l1[3])

"""
todo: Множества - уникальные значения
"""

set1 = {9, 9, 8, 8, 7, 7}
set1.add(1)
set1.update({8, 666, 1})
print(set1)

set2 = set() # пустое множество

"""
todo: Словари (dict)
"""
d1 = {} # пустой словарь

d2 = {
    'name': 'Yuriy',
    'skills': ('Python', 'Linux'),
}

print(d2['name'], d2['skills'][1])

"""
todo: как определить тип данных переменной в Python
"""
var = None
print(type(d2), type(s1))

"""
todo: Как выполнить явное приведение переменной
      к определенному типу данных в Python ?
"""

print(bool(s1), bool(d1))

"""
todo: Какие операторы существуют в Python
Арифметические:  + - * / % ** //
Сравнения:       == != > < >= <=
Присваивания:    = += -= *= /= %= **= //=
Побитовые:        &   |  ~  ^  << >> 
Логические:      and or not
Принадлежности:  in, not in
Тождественности: is, not is (для ссылочных типов)
"""

