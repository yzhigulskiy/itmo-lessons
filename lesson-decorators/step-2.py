def greeting_username(name):
    return f'Hello, {name}'


def goodbye(firstname, lastname):
    return f'Goodbye {firstname} {lastname}'


def exclaim_decorator(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs) + '!'
    return wrapper


greeting_username = exclaim_decorator(greeting_username)
goodbye = exclaim_decorator(goodbye)


print(greeting_username('Вася'))

print(goodbye('Вася', 'Пупкин'))