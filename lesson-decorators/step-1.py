# todo: Шаг №1. Дополняем работу функции, не изменяя оригинальную


def say_hello():
    return 'Hello'


def null_decorator(func): # функция-обертка
    return func


def exclaim_decorator(func):
    def wrapper():
        return func() + '!'
    return wrapper

say_hello = null_decorator(say_hello)

#print(say_hello, type(say_hello))
#print(say_hello())

say_hello = exclaim_decorator(say_hello)
print(say_hello, type(say_hello))
print(say_hello())