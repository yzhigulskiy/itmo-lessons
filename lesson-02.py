"""
todo: Ветвление
"""
# a = int(input("Enter first number: "))
# b = int(input("Enter second number: "))
#
# if a > b:
#     print('a > b')
#     if a is None:
#         pass
# elif a == b:
#     print('a == b')
# else:
#     print('a < b')

"""
todo: Циклы
break - мгновенно прервать работу цикла
continue - пропустить текущую итерацию
"""

i = 1
while i <= 10:
    i += 1
print(i)

j = 1

while True:
    if j >= 5:
        print(j)
        break
    j += 1

for i in range(5):
    print(i)

d = {
    'firstname': 'Vasya',
    'secondname': 'Pupkin',
    'age': 1000,
}

for i in d.items():
    print(i)

for key, value in d.items():
    print(key, value)

person = ('Vasya', 'Pupkin', 1000)

for i, v in enumerate(person):
    print(i, v)

"""
todo: Срезы
"""
s = 'Hello, Python!'
print(s[::-1])

"""
todo: Как не нужно генерировать строки!!!!!
"""

s = ''

for i in range(5):
    s += str(i)

for i in range(5):
    s.append(str(i))

s = ''.join(s)

