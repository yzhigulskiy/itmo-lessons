def recursion(number):
    if number <= 0:
        print("Finished")
    else:
        print(number)
        number -= 1
        recursion(number)


recursion(9)