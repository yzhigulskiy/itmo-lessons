"""
Генераторы

yield - функция генератор, возвращает результат, и останавливает прог. в этой точке по не будет вызван след. шаг
"""


def generator():
    print('Шаг №1')
    yield 1
    print('Шаг №2')
    yield 2
    print('Шаг №3')


g = generator()
print(g, type(g))
print(next(g))
print(next(g))
# print(next(g))


def countdown(n, step=1):
    print('Генератор стартовал!')
    while n > 0:
        yield n
        n -= step


for i in countdown(6, step=2):
    print(i)


