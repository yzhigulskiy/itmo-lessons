"""
args => кортеж

[*args]

Генераторы списков/множеств/словарей

[expression for item1 in iterable1 if conditional1
            for item2 in iterable12 if conditional2
            ...
            for itemN in iterableN if conditionalN ]
"""

numbers = [1, 1, 2, 2, 3, 3]

# todo: Генераторы списков
"""
square = []
for i in numbers:
    squares.append(i * i)
"""


squares = [i * i for i in numbers]
print(squares)

squares2 = map(lambda i: i * i, numbers)
print(*squares2)

"""
numbers_2 = []
for i in numbers:
    if i % 2:
        numbers_2.append(i)
"""

odd = [i for i in numbers if i % 2]
print(odd)


odd2 = list(filter(lambda i: i % 2, numbers))
print(odd2, 'test')


"""
points = []
for x in range(3):
    for y in range(2):
        points.append((x, y))
"""

points = [(x, y) for x in range(3) for y in range(2)]
print(points)


# todo: Генераторы множеств

ss = {i for i in numbers}
print(ss)
print(set(numbers))

# todo: Генераторы словарей

keys = ['id', 'name', 'age']
values = [1, 'Linux', 50]

# d = {k: v for i, k in enumerate(keys)
#           for j, v in enumerate(values) if i == j}
# print(d)

d = dict(zip(keys, values))
print(d)


# todo: Выражения-генераторы

squares = (i * i for i in numbers)
print(squares, type(squares), tuple(squares))


with open(__file__) as f:
    lines = (line.strip() for line in f)
    todos = (s for s in lines if s.startswith('# todo:'))
    print(list(todos))


