"""
Сопрограммы
"""


def coroutine(func):
    def wrapper(*args, **kwargs):
        g = func(*args, **kwargs)
        next(g)
        return g
    return wrapper


@coroutine
def echo():
    print('Генератор готов к приему сообщение')
    while 1:
        msg = yield
        print(msg)


g = echo()
next(g)
# отправить значение в генератор
g.send('Hello, echo generator!')
g.send('I like it!')

g.throw(RuntimeError, 'Тебе капец')

# внутри генератора возбуждается исключение GeneratorExit
g.close()

g.send('Opss =(')