"""
Итераторы
"""

s = 'Linus Torvalds'
lst = [1, 2, 3, 4, 5]
person = {
    'name': 'Linus Torvalds',
    'age': 42,
    'is_developer': True,
}

it = iter(s)
it = iter(lst)
it = iter(person)
it = iter(person.items())
print(it)
print(next(it))
print(next(it))
print(next(it)) # StopIteration


for key, value in person.items():
    print(key, value)


# Имитируем раоту цикла for
it = iter(person.items())
while 1:
    try:
        key, value = next(it)
        print(key, value)
    except StopIteration:
        break


with open(__file__) as f:
    print(f)
    print(iter(f))

    for line in f:
        pass