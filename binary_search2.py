def binary_search(lst, item):
    low = 0
    high = len(lst) - 1

    while low <= high:
        mid = int((low + high) / 2)
        guess = lst[mid]
        if guess == item:
            return mid
        if guess > item:
            high = mid - 1
        else:
        print('1')
    return None


lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

result = binary_search(lst, 2)
print(lst[result])