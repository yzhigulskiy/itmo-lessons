import sys
from textwrap import dedent
import pkg_resources

from lesson_utils import user_input, multi_line_input, input_datetime, input_int

from . import storage
from .services import make_menu


main_menu, main_menu_handler = make_menu()


def get_task():
    """Запрашивает идентификатор задачи и возвращает ее из БД"""
    task_id = input_int('Введите ID задачи', required=True)

    task = storage.get_task(task_id)
    if task is None:
        print(f'Задача с "{task_id}" не найденна.')
    return task


def input_task_data(task=None):
    """Запрашивает от пользователя данные о задаче"""
    task = dict(task) if task else {}
    data = {}

    data['title'] = user_input('Название', task.get('title'), required=True)

    data['planned'] = input_datetime(
        '%d.%m.%Y %H:%M', 'Запланированно', default=task.get('planned'), required=True
    )

    data['description'] = multi_line_input(
        'Описание', default=task.get('description')
    )


@main_menu('1', 'Вывести список задач')
def action_list_tasks():
    """Вывести список задач"""


@main_menu('2', 'Добавить задачу')
def action_add_task():
    """Добавить задачу"""

    data = input_task_data()
    task = storage.create_task(**data)

    print(f'''Задача "{task['title']}" успешно созданна.''')


@main_menu('3', 'Отредактировать задачу')
def action_edit_task():
    """Отредактировать задачу"""
    task = get_task()
    
    if task is None:
        return

    data = input_task_data(task)
    storage.update_task(task['id'], **data)
    print(f'''Задача "{title}" успешно отредактированна.''')


@main_menu('4', 'Завершить задачу')
def action_done_task():
    """Завершить задачу"""


@main_menu('5', 'Начать новую задачу')
def action_reopen_task():
    """Начать задачу заново"""


@main_menu('q', 'Выйти')
def action_exit():
    """Выйти"""
    sys.exit(0)


def main():

    schema_path = pkg_resources.resource_filename(__name__, 'resources/schema.sql')
    storage.initialize(schema_path)

    main_menu_handler('m')

    while 1:
        cmd = input('\nВведите команду: ')
        main_menu_handler(cmd)