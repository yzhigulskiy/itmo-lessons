from setuptools import setup, find_packages


setup(
    name='task-bookkeeping',
    version='1.0.0',
    description='Bookkeeping.',
    license='Apache License 2.0',
    author='Yuriy Zhigulskiy',
    author_email='yzhigulskiy@gmail.com',
    packages=find_packages(),
)