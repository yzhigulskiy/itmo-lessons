sql = '''
    CREATE TABLE IF NOT EXISTS payments (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        payment_title TEXT NOT NULL,
        payment_price TEXT NOT NULL,
        quantity TEXT,
        created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
'''
def test():
    print('test')


sql_payment_insert = '''
    INSERT INTO payments (payment_title, payment_price, quantity, created_date) VALUES (?, ?, ?, ?)
'''


sql_payment_update = '''
    UPDATE payments SET 
        payment_title=?, payment_price=?, quantity=?, created_date=?
    WHERE id=?
'''


sql_select_payments_for_period = '''
    SELECT id, payment_title, created_date WHERE created_date=
'''


sql_top_payments = '''
    SELECT id, payment_title, payment_price FROM payments limit 10
'''