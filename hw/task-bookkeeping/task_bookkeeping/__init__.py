import sys
import sqlite3
from task_bookkeeping import storage as st

def add_payment():
    print('\nДобавить платеж')
    user_input = input('Введите данные платежа: ')
    if len(user_input.split(', ')) < 4:
        lst = user_input.split(', ')[0:2]
        lst.append('NULL')
        lst.append(user_input.split(', ')[-1])
        sql_data = tuple(lst)
    else:
        sql_data = tuple(user_input.split(', '))
    with sqlite3.connect('db.sqlite') as conn:
        conn.execute(st.sql)
        conn.execute(st.sql_payment_insert, sql_data)

    print('ОК')

def update_payment():
    print('\nОтредактировать платеж')


def print_payments_for_period():
    print('\nВывести все платежи за указанный период')



def top_payments():
    print('\nВывести топ самых крупных платежей')
    with sqlite3.connect('db.sqlite') as conn:
        cursor = conn.execute(st.sql_top_payments)
        tasks = cursor.fetchall()

    for task in tasks:
        print(task)

def print_menu():
    print('')
    for key in actions:
        print(f'{key}. {actions[key][0]}')


def close_program():
    sys.exit(0)


actions = {
    '1': ('Добавить платеж', add_payment),
    '2': ('Отредактировать платеж', update_payment),
    '3': ('Вывести все платежи за указанный период', print_payments_for_period),
    '4': ('Вывести топ самых крупных платежей', top_payments),
    '5': ('Показать меню', print_menu),
    '6': ('Закрыть программу', close_program),
}


def main():
    print_menu()
    while True:
        cmd = input('\nВведите команду: ')
        action = actions.get(str(cmd))[1]
        if action:
            action()
        else:
            print('Не корректная команда')
