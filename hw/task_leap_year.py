try:
    year = int(input())
    if year % 100 == 0 and year % 400 != 0:
        print('no')
    elif year % 4 == 0:
        print('yes')
    else:
        print('no')
except ValueError as err:
    print(err)
