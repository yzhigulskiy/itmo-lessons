import json


def return_correct_value(dict1, v):
    if type(dict1) is not list:
        lst = dict1, v
        dict1 = list(lst)
    else:
        dict1.append(v)
    return dict1


def line_to_dict(lines):
    dict1 = {}
    for line in lines:
        if line == '\n':
            continue
        elif line[0:6] == 'HTTP/2':
            dict1.update({'protocol': 'HTTP/2', 'status_code': line.split()[1]})
        elif line[0:6] == 'HTTP/1':
            dict1.update({'protocol': line.split()[0], 'status_code': line.split()[1],
                          'status_message': ' '.join(line.split()[2:])})
        elif 'POST' in line[0:4] or 'GET' in line[0:4]:
            dict1.update({'method': line.split()[0], 'uri': line.split()[1],
                          'protocol': line.split()[2]})
        else:
            if len(line.split(': ')) > 1:
                key, value = line.split(': ')
            else:
                key, value = line.split(': ') + [' ']
            if value.endswith("\n"):
                value = value[:-1]
            if dict1.get(key):
                dict1[key] = return_correct_value(dict1[key], value)
            else:
                dict1.update({key: value})
    return dict1


def http_headers_to_json(path_to_http_headers, path_to_result_json):
    json_result = {}
    with open(path_to_http_headers) as f:
        json_result = line_to_dict(f)

    with open(path_to_result_json, 'w') as f:
        json.dump(json_result, f)
