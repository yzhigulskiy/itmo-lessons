def camel_to_snake(name):
    name2 = ''
    for i in name:
        if i.isupper():
            name2 += '_' + i.lower()
        else:
            name2 += i
    return name2[1:]


def snake_to_camel(name):
    name2 = []
    for i in name.split('_'):
        name2.append(i.capitalize())
    name2 = "".join(name2)
    return name2
