import datetime


def counter():
    new_year = datetime.datetime.today().year + 1
    days_left = datetime.datetime(new_year, 1, 1) - datetime.datetime.today()
    days = days_left.days
    hours = int(days_left.seconds / 60 / 60)
    mins = int((days_left.seconds / 60) - ((int(days_left.seconds / 60 / 60)) * 60))

    if int(str(days)[-1:]) == 1 and not 10 < int(days) < 14:
        word_days = "день"
    elif 1 < int(str(days)[-1:]) < 4 and not 10 < int(days) < 14:
        word_days = "дня"
    else:
        word_days = "дней"

    if int(str(hours)[-1:]) == 1 and not 10 < int(hours) < 14:
        word_hours = "час"
    elif 1 < int(str(hours)[-1:]) < 4 and not 10 < int(hours) < 14:
        word_hours = "часа"
    else:
        word_hours = "часов"

    if int(str(mins)[-1:]) == 1 and not 10 < int(mins) < 14:
        word_mins = "минута"
    elif 1 < int(str(mins)[-1:]) < 4 and not 10 < int(mins) < 14:
        word_mins = "минуты"
    else:
        word_mins = "минут"

    return f'{days} {word_days} {hours} {word_hours} {mins} {word_mins}'
