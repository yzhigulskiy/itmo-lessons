def average(lst):
    if len(lst) > 0:
        return round(sum(lst) / len(lst), 3)
    else:
        raise ArithmeticError('The list must contain at least one item')



