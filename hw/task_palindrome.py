def is_palindrome(s):
    s2 = ''
    s = str(s)
    for i in s:
        if i.isalpha() or i.isdecimal():
            s2 += i.lower()

    for i in range(len(s2) // 2):
        if s2[i] != s2[::-1][i]:
            return False
    return True

