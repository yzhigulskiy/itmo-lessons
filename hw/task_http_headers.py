import json


def return_correct_value(dict1, v):

    print(dict1, v)
    if type(dict1) is not list:
        lst = dict1, v
        dict1 = list(lst)
    else:
        dict1.append(v)
    return dict1


def http_headers_to_json(path_to_http_headers, path_to_result_json):
    json_result = {}
    with open(path_to_http_headers) as f:
        for line in f:
            if line == '\n':
                continue
            if line.startswith('HTTP/2'):  # use startwith()
                json_result = {'protocol': 'HTTP/2',
                               'status_code': line.split()[1]}
            elif line.startswith('HTTP/1'):
                json_result = {'protocol': line.split()[0],
                               'status_code': line.split()[1],
                               'status_message': ' '.join(line.split()[2:])}
            elif line.startswith('POST') or line.startswith('GET'):
                json_result = {'method': line.split()[0],
                               'uri': line.split()[1],
                               'protocol': line.split()[2]}
            else:
                if len(line.split(': ')) > 1:
                    k, v = line.split(': ')
                else:
                    k, v = line.split(': ') + [' ']
                v = v.replace('\n', '')
                if k in json_result:
                    json_result[k] = return_correct_value(json_result[k], v)
                else:
                    json_result.update({k: v})
    with open(path_to_result_json, 'w') as f:
        json.dump(json_result, f)