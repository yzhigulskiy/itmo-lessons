try:
    plates = int(input())
    detergent = int(input())

    while (plates and detergent):
        detergent -= 0.5
        plates -= 1

    if detergent == 0 and plates != 0:
        print("Моющее средство закончилось. Осталось " + str(plates) + " тарелок")
    elif plates == 0 and detergent != 0:
        print("Все тарелки вымыты. Осталось " + str(detergent) + " ед. моющего средства")
    elif (plates and detergent) == 0:
        print("Все тарелки вымыты, моющее средство закончилось")

except ValueError as err:
    print(err)
