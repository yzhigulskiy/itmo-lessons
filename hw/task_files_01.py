result1 = []
result2 = []
with open('data.txt') as f:
    n = int(input())
    p = int(input())
    result = f.readline()
    for i in result.split():
        i = int(i)
        if i % n == 0:
            result1.append(str(i))
            with open('out-1.txt', 'w') as f:
                f.write(' '.join(result1))
        else:
            result2.append(str(int(i ** p)))
            with open('out-2.txt', 'w') as f:
                f.write(' '.join(result2))