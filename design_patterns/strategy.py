"""
Strategy (Стратегия)

"""


from abc import ABCMeta, abstractmethod


class CompressStrategy(metaclass=ABCMeta):
    @abstractmethod
    def compress(self):
        pass


class ZipCompressStrategy(CompressStrategy):
    def compress(self):
        print('ZIP')


class GzipCompressStrategy(CompressStrategy):
    def compress(self):
        print('GZIP')


class Archive(object):
    def __init__(self, compress_strategy):
        self.compress_strategy = compress_strategy

    def compress(self):
        self.compress_strategy.compress()


a = Archive(
    compress_strategy=ZipCompressStrategy()
)
a.compress()
