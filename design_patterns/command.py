"""
Command (Команда) - Поведенческий 

"""

from abc import ABCMeta, abstractmethod


class Command(metaclass=ABCMeta):
    @abstractmethod
    def execute(self):
        pass


class HelloCommand(Command):
    def execute(self):
        print('Hello!')


if __name__ == '__main__':
    cmd = HelloCommand()
    cmd.execute()


