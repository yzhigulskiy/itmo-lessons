"""
State (Состояние)
"""

from abc import ABCMeta, abstractmethod


class State(metaclass=ABCMeta):
    @abstractmethod
    def attack(self, wolf):
        """Отразить нападение волка"""

    @abstractmethod
    def eat(self):
        """Покормить фермера"""

    @abstractmethod
    def feed_animals(self):
        """Покормить животных"""


class DreamState(State):
    """Состояние сон"""
    def attack(self, wolf):
        print('Фермер не может отразить нападение волков.')

    def eat(self):
        print('Фермеру снится барашек, но он голоден.')

    def feed_animals(self):
        print('Ночью нужно спать, а не кушать')


class AttackState(State):
    """Состояние Атака на волка"""
    def attack(self, wolf):
        print('Фермер боррется с волками')

    def eat(self):
        print('Если бы не волки, я бы перекусил!')

    def feed_animals(self):
        print('Придется потерпеть, иначе нас всех съедят волки')


class AwakeState(State):
    def attack(self, wolf):
        print('На ферме тихо, волков нет.')

    def eat(self):
        print('Чтобы перекусить?')

    def feed_animals(self):
        print('Кушайте мои хорошие.')


class Farmer(object):
    def __init__(self, state):
        self._state = state

    def attack(self, wolf):
        self._state.attack(wolf)

    def eat(self):
        self._state.eat()

    def feed_animals(self):
        self._state.feed_animals()

    def change_state(self, state):
        self._state = state


farmer = Farmer(AwakeState())
farmer.eat()
farmer.feed_animals()

farmer.change_state(AttackState())
farmer.eat()
farmer.attack(None)

farmer.change_state(DreamState())
farmer.attack(None)
farmer.feed_animals()
