"""
Singletone (Пораждающие)

гарантирует, что у класса есть только один экземпляр, и предоставляет к нему глобальную точку доступа.
"""


from configparser import ConfigParser


class SingletonMeta(type):
    __instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.__instances:
            cls.__instances[cls] = super().__call__(*args, **kwargs)
        return cls.__instances[cls]


class Config(metaclass=SingletonMeta):
    __slots__ = ('config', )
    def __init__(self):
        self.config = ConfigParser()

    def __getattribute__(self, name):
        try:
            return super().__getattribute__(name)
        except AttributeError:
            return getattr(self.config, name)


config1 = Config()
config1.add_section('main')
config1.set('main', 'debug', '1')

config2 = Config()
debug = config2.getboolean('main', 'debug')
print(f'Debug = {debug}')

print(config1 is config2)
