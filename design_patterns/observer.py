"""
Observer (Наблюдатель)
Поведенческий
"""

from abc import ABCMeta, abstractmethod
from random import randrange


class Subject(object):
    """Субъект - тот, кто создает событие
                 за кем ведется наблюдение
    """
    def __init__(self):
        self._observers = []

    def add_observer(self, observer):
        self._observers.append(observer)

    def remove_observer(self, observer):
        if observer in self._observers:
            self._observers.remove(observer)

    def notify_observers(self):
        for observer in self._observers:
            observer.update()


class ObserverInterface(metaclass=ABCMeta):
    """Наблюдатель - тот, наблюдает за субъектом и реагирует на изменение.
    """
    @abstractmethod
    def update(self):
        pass


class LoginHandler(Subject):
    def __init__(self):
        super().__init__()
        self.result = None

    def authorize(self):
        """
        Результат работы метода:
        1. Успешный вход
        2. Ошибка при входе

        Требования:
        1. Логировать все успешные и ошибочные попытки входа
        2. Логировать все ошибочные попытки
        3. Ошибка с провайдером, нужно послать Cookies
        """
        self.result = randrange(3)
        self.notify_observers()


class Observer(ObserverInterface):
    def __init__(self, subject):
        self.subject = subject


class LoggerObserver(Observer):
    def update(self):
        print('Пишем в access.log')


class ErrorObserver(Observer):
    def update(self):
        if self.subject.result == 1:
            print('Пишим в error.log')


class CookieObserver(Observer):
    def update(self):
        if self.subject.result == 2:
            print('Отправляем Cookie в файлы')


login_handler = LoginHandler()

login_handler.add_observer(
    LoggerObserver(login_handler)
)

login_handler.add_observer(
    ErrorObserver(login_handler)
)

login_handler.add_observer(
    CookieObserver(login_handler)
)

login_handler.authorize()
