"""
Factory Mathod (Фабричный шаблон) - Порождающий

Определяет интерфейс создания объекта, но оставляет подклассам решение о том, какой класс инстанциировать 
Позволяет 
"""

from abc import ABCMeta, abstractmethod


class LoggerInterface(metaclass=ABCMeta):
    @abstractmethod
    def log(self, msg):
        pass


class StdoutLogger(LoggerInterface):
    def log(self, msg):
        print(msg)

    
class FileLogger(LoggerInterface):
    def __init__(self, filename):
        self.filename = filename

    def log(self, msg):
        with open(self.filename, 'a') as f:
            f.write(f'{msg}]\n')


class LoggerFactory(metaclass=ABCMeta):
    @abstractmethod
    def create_logger(self):
        pass


class StdoutLoggerFactory(LoggerFactory):
    def create_logger(self):
        return StdoutLogger()


class FileLoggerFactory(LoggerFactory):
    def __init__(self, filename):
        self.filename = filename

    def create_logger(self):
        return FileLogger(self.filename)


class Application(object):
    def __init__(self, logger_factory=None):
        self.logger_factory = logger_factory or StdoutLoggerFactory()

    def create_logger(self):
        return self.logger_factory.create_logger()

    def log(self, msg):
        self.create_logger().log(msg)


#app = Application()
app = Application(
    logger_factory=FileLoggerFactory('log.txt')
)
app.log('Сообщение в лог')

