"""
Chain of responsibility
Поведенческий 
"""


from abc import ABCMeta, abstractmethod
import os


class Decoder(metaclass=ABCMeta):
    _available_extensions = ()

    def __init__(self):
        self._next = None

    def set_next(self, next_):
        self._next = next_

    def _get_extension(self, filename):
        _, ext = os.path.splitext(filename)
        return ext.strip('.')

    @abstractmethod
    def _do_decode(self, filename):
        pass

    def decode(self, filename):
        ext = self._get_extension(filename)

        if ext in self._available_extensions:
            return self._do_decode(filename)

        if self._next is not None:
            return self._next.decode(filename)

        return False


class MkvDecoder(Decoder):
    _available_extensions = ('mkv', )

    def _do_decode(self, filename):
        print(f'Decode file {filename} in mkv format.')
        return True


class Mp3Decoder(Decoder):
    _available_extensions = ('mp3', )
    def _do_decode(self, filename):
        print(f'Decode file {filename} in mp3 format.')
        return True


class MpegDecoder(Decoder):
    _available_extensions = ('mp4', 'mpeg')
    def _do_decode(self, filename):
        print(f'Decode file {filename} in mp4 format.')
        return True


class Player(object):
    def __init__(self):
        self.decoder = None

    def init_decoders(self, decoder, *decoders):
        self.decoder = last = decoder

        for decoder in decoders:
            last.set_next(decoder)
            last = decoder

    def play(self, filename):
        return self.decoder.decode(filename)


player = Player()
player.init_decoders(
    MkvDecoder(), MpegDecoder(), Mp3Decoder()
)
player.play('video.mp4')
player.play('audio.mp3')
player.play('video.mkv')
player.play('audio.wma')


