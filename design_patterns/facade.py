"""
Facade (Фасад)
Структурный 
"""


import socket


class TCPServer(object):
    def __init__(self, host='localhost', port=0, backlog=2):
        self.address = (host, port)
        self.backlog = backlog

    def make(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(self.address)
        sock.listen(self.backlog)
        return sock


server = TCPServer()

with server.make() as sock:
    conn, addr = sock.accept()
