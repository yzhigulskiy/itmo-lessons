"""
Template  Method (Шаблонный метод)
Поведенческий

"""
from abc import abstractmethod
from command import Command as _Command


class Command(_Command):
    @abstractmethod
    def _do_execute(self):
        pass

    def execute(self):
        print(f'Действия до выполнения команды')
        self._do_execute()
        print(f'Действия после выполнения команды')


class CreateTaskCommand(Command):
    def _do_execute(self):
        print('Добавить новую задачу')


class EditTaskCommand(Command):
    def __init_(self, pk):
        self.pk = pk

    def _do_execute(self):
        print(f'Изменить задачу с ID: {self.pk}')


if __name__ == '__main__':
    cmd1 = CreateTaskCommand()
    cmd2 = EditTaskCommand(1)

    cmd1.execute()
    cmd2.execute()
