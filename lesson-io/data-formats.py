"""
Форматы данных
"""

data = {
    'users': [
        {
            'id': 1,
            'name': 'Linus Torvalds',
            'skills': ('C++', 'Linux'),
            'is_student': False,
        },
        {
            'id': 2,
            'name': 'Richard Stallman',
            'skills': ('GNU', 'C', 'C++'),
            'is_student': False,
        },
    ],
}


# todo: Pickle

import pickle


with open('users.pickle', 'wb') as f:
    pickle.dump(data, f)


with open('users.pickle', 'rb') as f:
    loaded_data = pickle.load(f)
    print(f'Прочитанно из Pickle: {loaded_data}')


# JSON (Java Script Object Notation)

import json


with open('users.json', 'w') as f:
    json.dump(data, f, indent=4)


with open('users.json') as f:
    loaded_data = json.load(f)
    print(f'Прочитанно из JSON: {loaded_data}')


"""
# doto: CSV (Comma Separated Value)

id;name;skiils;is_student
1;Linus Torvalds;C++,Linux;1
2;Richard Stallman;GNU,C;1
"""

import csv


with open('users.csv', 'w') as f:
    users = data.get('users', [])

    if users:
        fieldnames = users[0].keys()
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(users)


with open('users.csv') as f:
    reader = csv.DictReader(f)
    users = list(reader)
    print(f'Прочитанно из CSV: {users}')


"""
todo: INI - модуль configparser

debug 1
db.host localhost
db.user root

[db]
host localhost
user root
"""

"""
todo: XML - модуль lxml

<users>
    <user>
        <id>1</id>
        <name>Linus Torvalds</name>
        <skills>
            <skill>C++</skill>
            <skill>Linux</skill>            
        </skills>
    </user>
    <user id="1" name="Linus Torvalds">
        <skills>
            <skill name="C++" />
            <skill name="Linux" />
        </skills>
    </user>    
</users>
"""


# Yml/Yaml - модуль

