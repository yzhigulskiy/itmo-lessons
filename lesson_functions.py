"""
Функции
"""

"""
todo: Как объявить функцию
"""
def say_hello():
    print("Hello!")


say_hello()

"""
todo: Как вернуть значение из функции ?
"""


def get_hello():
    return "Hello", "test"


result = get_hello()
print(result)


"""
todo: Как передать функции аргументы
"""


def print_greeting(username):
#   print("Hello, {0}".format(username))
    print('Hello, ', username, '!', sep='')


print_greeting("Yuriy")


def multi(a, b):
    return a * b


c = multi(5, 8)
print(c)


"""
todo: Как задать значения аргументов по умолчанию ?
"""


def my_pow(x, p=2):
    return x ** p


print(my_pow(2))
print(my_pow(2, 3))


"""
todo: Позиционные и именованные аргументы.
"""

print(my_pow(p=4, x=7))
print(my_pow(6, p=8))


"""
todo: Переменное количество переменных
"""


def multi_cool(a, b, *numbers):
    print(a, b, type(numbers), numbers)
    result = a * b
    for i in numbers:
        result *= 1

    return result


#print(multi_cool())
#print(multi_cool(1))
print(multi_cool(1, 2))
print(multi_cool(1, 2, 3))


def db_connect(provider, **options):
    print('Connecting to DB:', provider)
    print('===>', type(options), options)


db_connect('sqlite', filename='db.sqlite')
db_connect('mysql', host='localhost', user='root', port=666)


"""
todo: Как развернуть кортеж/список в значения
      позиционных аргументов?
"""

lst = [8, 20]
print(multi(*lst))

lst = range(1, 11)
print(multi_cool(*range(1, 11)))


"""
todo: Как развернуть словарь в значения 
      именнованных аргументов?
"""

config = {
    'provider': 'mysql',
    'host': '127.0.0.1',
    'user': 'root',
    'password': 'toor',
}

db_connect(**config)


"""
todo: Явное различие аргументов в Python3
"""


def demo_arguments(a, b=None, *c, d, e=None, **f):
    print(a, b, c, d, e, f)


demo_arguments(1, d=2)
demo_arguments(1, 2, d=3, e=4)
demo_arguments(1, 2, 3, 4, 5, d=10, e=20, key=30)


"""
todo: Передача значений аргументов по ссылке
"""


def split_pieces(s, size, output=None):
    if output is None:
        output = []

    start = 0
    end = len(s)

    while start < end:
        output.append(s[start:start+size])
        start += size

    return output


s = 'У Васи было пять яблок, а Пети 2, что это?'
found = []
split_pieces(s, 4, found)
print(found)

found_2 = split_pieces(s, 4)
print(found_2)

'''
def demo_default(x, lst=None):
    print(lst)
    lst.append(x ** 2)
    print(lst)


demo_default(2)
demo_default(4)
'''

"""
todo: Анонимные функции 
Callbacl - функция обратного вызова
"""



#sqrt1 = lambda x: x ** .5
#print(sqrt1(9))


"""
todo: Рекурсивная функция
Прямая
Косвенная:
def(a):
    b()
    
def(b):
    a()
<если_истинна> if <условие> else <если_ложь>
"""


def factorial(n):
    return 1 if n == 0 else n * factorial(n - 1)


"""
todo: Замыкания
"""

# Пример функции каррирования
def trim(chars=None):
    # Замкнутая область видимости
    print(chars)

    def f(s):
        return s.strip(chars)
    return f


trim_spaces = trim()
trim_slashes = trim('/|\\')

print(trim, type(trim_spaces))

print(trim_spaces, trim_spaces("          2345           "))
print(trim_slashes, trim_slashes('///////////post\\|||||||'))

"""
todo: Область видимости
Глобальная область видимости
Локальная область видимости
"""


g = 666


def f():
    external = 777

    def func():
        global g
        nonlocal external # только если мы хотим изменить эту переменную, для чтения не нужно
        g += 1
        external += 1

    func()
    print(g, external)


f()