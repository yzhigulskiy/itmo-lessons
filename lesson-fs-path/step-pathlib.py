'''
Рекурсивный поиск и замена по директории 

Найти в директории все html фалы и заменить email:
username@gmail.com -> DELETED@gmail.com
'''

from pathlib import Path

# Определяем путь поиска файлов
p = Path('/home/yuriy/git/itmo-lessons/lesson-fs-path/www/')

# Определяем что мы хотим найти
filenames = list(p.glob('**/*.html'))

# Проходим циклом по всем файлам
for filename in filenames:
    print(filename)
    with filename.open() as f:
        f.readline()
        f.seek(0)
        for line in f:
            print(line.strip())


# Получаем текущую дерикторию
Path.cwd()

# Получаем домашнюю директорию текущего пользователя
Path.home()

# Получение информации о файле
p = Path('step-1.py')
p.stat().st_mode
p.stat().st_ino
p.stat().st_uid
p.stat().st_atime

# Смена прав на файл
# p.chmod(0o444)

# Проверка существования файла
p.exists()

# Получаем список файлов в директории
p = Path('/home/yuriy')
for files in p.iterdir():
    print(files)
