"""
Сразу прошу прощение про сумбур в задании т.к. сформулировал его по памяти:

Есть 2 массива данных T, R
T - список с названием пройденных тестов ["test1a", "test2", "test1b", "test1c", "test3"]
R - список c результатом пройденных тестов ["Wrong answer", "OK", "Runtime error", "OK", "Time limit exceeded"]
т.е. студент сдал только "test2" и "test1с"

Требуется посчитать итоговый результат, который получил студент на экзамене при условии
что в массиве T хранятся группы тестов. Например:

T[0] - "test1a"  R[0] - "Wrong answer"
T[1] - "test2"   R[1] - "OK"
T[2] - "test1b"  R[2] - "Runtime error"
T[3] - "test1c"  R[3] - "OK"
T[4] - "test3"   R[4] - "Time limit exceeded"

Если в группе более одного теста то тесты именуются в окончании буквой. Т.е. в примере выше 3 группы тестов:
test1a, test1b, test1c - это одна группа тестов
test2 - другая группа
test3 - третья группа

Результат посчитать по формуле: ((количество успешных групп тестов * 100) /  количество групп) т.е.:
студент успешно сдал только группу тестов "test2" т.е. одну группу из трех (1 * 100) / 3 = 33.3
Полученный результат округлить до 33
"""

#T = ["test1a", "test2", "test1b", "test1c", "test3"]
#R = ["Wrong answer", "OK", "Runtime error", "OK", "Time limit exceeded"]
T = []
R = []
def solution(T, R):
    test_group = {}
    for test_name in T:
        if test_name[-1:].isdigit():
            if R[T.index(test_name)] == '"OK"':
                test_group[test_name] = 1 # test True
            else:
                test_group[test_name] = 0 # test False
        else:
            if test_name[:-1] not in test_group:
                if R[T.index(test_name)] != '"OK"':
                    test_group[test_name[:-1]] = 0
                else:
                    test_group[test_name[:-1]] = 1
            else:
                if test_group[test_name[:-1]] == 1 and R[T.index(test_name)] != '"OK"':
                    test_group[test_name] == 0
    success_group_test = 0
    total_test_count = 0
    for test in test_group.keys():
        total_test_count += 1
        if test_group[test] == 1:
            success_group_test +=1

    return round(success_group_test * 100 / total_test_count)


with open('test_input.txt') as fd:
    for line in fd:
        T.append(line.split(" ", 1)[0])
        R.append(line.split(" ", 1)[1].strip())


print(solution(T, R))