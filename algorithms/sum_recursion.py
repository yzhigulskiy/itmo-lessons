def quicksort(array):
    smaller = []
    greater = []
    if len(array) < 2:
        return array
    else:
        pivot = array[0]
        for i in array[1:]:
            if i < array[0]:
                smaller.append(i)
            else:
                greater.append(i)
        return quicksort(smaller) + [pivot] + quicksort(greater)

print(quicksort([10, 100, 150, 1, 1, 5, 5,   5, 2, 3]))
