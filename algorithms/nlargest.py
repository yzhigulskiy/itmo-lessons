import heapq

nums = [1, 1, 1, 2, 2, 3,1,4,5,6,3,3,5,5,6,2,2,3,4,3,3,5,3,3,5,5,6,6,8,2,1,3,2,4,5,3,2,2,2,1,3,4,5,6,6,4,3,2,4,5,4,5,3,5,3,5,4,3,5,3]
k = 2


def get_largest(nums, k):
    count = {}
    for i in nums:
        if i in count.keys():
            count[i] = count[i] + 1
        else:
            count[i] = 1
    print(count)
    return heapq.nlargest(k, count, key=count.get)


print(get_largest(nums, k))