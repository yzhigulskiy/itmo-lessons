from collections import deque

list1 = [1, 2, 4, 5, 6]
list2 = [1, 1, 1, 3, 4]

new_list = []


def merge_lists(h1, h2):
    while h1 and h2:
        if h1[0] <= h2[0]:
            new_list.append(h1[0])
            h1 = h1[1:]
        else:
            new_list.append(h2[0])
            h2 = h2[1:]

    return new_list + h1 + h2

print(merge_lists(list1, list2))