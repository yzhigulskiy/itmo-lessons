"""
Классы и объекты.

У объекта есть:
- свойства/атрибут/член/данные
- методы/поведение

self - ссылка на текущий объект/экземпляр

Модификаторы доступа:
public      - доступен из любой области
protected   - доступ внутри класса и классов-наследников
private     - доступ только внутри класса

prop, method     => public
_prop, _method   => protected
__prop, __method => private
"""


# todo: Как создать класс в Python?
# todo: Как объявить свойства в классе?
# todo: Как объявить метод в классе?
# todo: Зачем нужен коструктор?

class Person(object):
    def __init__(self, firstname, lastname, middlename=''):
        """Конструктор"""
        self.firstname = firstname
        self.lastname = lastname
        self.middlename = middlename

    def get_firstname(self):
        """Getter/Получатель"""
        return self.firstname

    def get_lastname(self):
        return self.lastname

    def get_middlename(self):
        return self.middlename

    def set_firstname(self, firstname):
        """Setter/Установщик"""
        self.firstname = firstname

    def set_lastname(self, lastname):
        self.lastname = lastname

    def set_middlename(self, middlename):
        self.middlename = middlename

    def get_fullname(self):
        return ' '.join(
            (self.firstname, self.lastname, self.middlename)
        )


# todo: Как создать объект (экземпляр) в Python?
person_1 = Person('Вася', 'Пупкин')
print(person_1, type(person_1))

person_2 = person_1
print(person_2, person_1 is person_2)


# person_1.firstname = 'Вася' # запись
# person_2.lastname = 'Пупкин' # запись


# чтение или получение значения св-ва.
print(f'Hello, {person_1.firstname} {person_1.lastname}')


person_3 = Person('Петр', 'I')

print(person_1.get_fullname())
print(person_3.get_fullname())


# todo: Что такое наследование

class Developer(Person):
    def __init__(self, firstname, lastname, middlename=''):
        super().__init__(firstname, lastname, middlename)
        self.skills = []

    def add_skill(self, skill):
        self.skills.append(skill)

    def get_skills(self):
        return self.skills


linus = Developer('Linus', 'Torvalds')
linus.add_skill('C')
linus.add_skill('Linux')
print(linus.get_skills())
print(linus.get_fullname())

# Рассказ случайного революционера


