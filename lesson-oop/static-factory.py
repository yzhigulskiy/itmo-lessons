"""
Статические свойства и методы
статические методы
"""


class CarError(Exception):
    pass


class Driver(object):
    def __init__(self, name):
        self.name = name


class Car(object):
    def __init__(self, number, vendor, driver=None):
        self.number = number
        self.vendor = vendor
        self.driver = driver

    def get_driver(self):
        return self.driver

    def set_driver(self, driver):
        self.driver = driver

    def has_driver(self):
        return self.get_driver() is not None

    def drive(self):
        if not self.has_driver():
            raise CarError("Can't drive without driver.")
        print('=======>>>>>>>>')


class CarFactory(object):
    __cars = {} # статическое св-во

    @classmethod
    def create(cls, number, vendor):
        """Возвращает экземпляр Car"""
        if number in cls.__cars:
            raise CarError(f'Car with number "{number}" already exist.')

        cls.__cars[number] = Car(number, vendor)
        return cls.__cars[number]

    @classmethod
    def get(cls, number):
        return cls.__cars.get(number)


car_1 = CarFactory.create('c123cc', 'Lada')
print(car_1)


try:
    car_2 = CarFactory.create('c123cc', 'Lada')
    print(car_2)
except CarError as err:
    print(err)
    car_2 = CarFactory.get('c123cc')
    print(car_1 is car_2)


driver = Driver('Вася')
car_1.set_driver(driver)
car_1.drive()