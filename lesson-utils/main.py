"""
todo: Модули
1. Ищет модули в текущей директории
2. PYTHONPATH
3. Специальные каталоги
Пространство имен модулей:
"""
"""
todo: Как импортировать модуль
1. Импорт модуля целиком
"""
import sys
import input_utils

# print(sys.path)
# a = input_utils.input_int()

"""
todo: Частичный импорт
"""

from input_utils import input_float, input_bin

# f = input_float()
# b = input_bin

"""
todo: 3. Импорт со *
"""

from input_utils import *

"""
todo: Как задать пресвдонимы
"""

from input_utils import user_input as base_user_input

#asb = base_user_input()

import os.path as path


# import demo
#
# print(demo.is_debug())
# demo.debug = True
# print(demo.is_debug())

from demo import debug, is_debug

print(is_debug())
debug = True
print(is_debug())

"""
todo: Главный (исполняемый) модуль
"""