lst = []
print(type(lst), type(list))


class MyList(list):
    def get(self, index, default=None):
        return self[index] if index in self else default


print(type(MyList))

my_lst = MyList()
my_lst.append(1)
my_lst.append(2)
my_lst.append(3)

print(type(my_lst), type(MyList))



"""
type(obj)
type(name, bases, dict)
    name  - имя нового типа
    bases - кортеж базовых типов
    dict  - словарь атрибутов создаваемого типа
"""


def person_init(self, firstname, lastname):
    self.firstname = firstname
    self.lastname = lastname


Person = type('Person', (object,),
              {'__init__': person_init,
               'get_firstname': lambda self: self.firstname,
})

print(Person, type(Person))