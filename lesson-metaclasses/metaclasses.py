"""
Метаклассы

type - метакласс,
        по умолчанию, является базовым метаклассом для всех классов.
"""



class DemoMeta(type):
    def __new__(mcs, name, bases, d):
        print(f'Выделение памяти под класс:\n{name} {bases} {d}')
        return super().__new__(mcs, name, bases, d)

    def __init__(cls, name, bases, d):
        print(f'Инициализация класса:\n{name} {bases} {d}')
        super().__init__(name, bases, d)

    def __call__(cls, *args, **kwargs):
        print(f'Создание экземпляра (объекта):\n{args} {kwargs}')
        return super().__call__(*args, **kwargs)


class Demo(metaclass=DemoMeta):
    def __new__(cls, *args, **kwargs):
        print(f'Выделение памяти под объект:\n{args} {kwargs}')
        return super().__new__(cls)

    def __init__(self, *args, **kwargs):
        print(f'Инициализация объекта:\n{args} {kwargs}')


demo_obj = Demo()

demo_obj_2 = Demo(1, 2, 3, k=4)
