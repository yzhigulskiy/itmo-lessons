"""
1. Специальные св-ва и методы классов
__name__     - имя класса
__qualname__ - полное имя класса, метода, функции
__class__    - объект-метакласс
__module__   - имя модуля, где объявлен класс
__bases__    - кортеж базовых типов
__dict__     - словарь атрибутов класса
__doc__      - строка документации

2. Специальные св-ва и методы объектов
__class__  - объект-класс, экземпляром которого явл. данный объект
__dict__   - словарь атрибутов объекта (св-ва, созданные в конструкторе)
__dir__()  - список атрибутов класса => dir(obj)
__repr__() - строковое представление объекта => repr()

3. Явное привидение объекта к определенному типу
__bool__()    - преобразование в логический тип => bool(obj)
__int__()     - преобразование в целочисленный тип => int(obj)
__index__()   - преобразование без потерь числового объекта в целочисленный тип
                => срезы => [:obj]
                => обращение по индексу => lst[obj]
                => bin(), oct(), hex()
                => перегрузка оператора operator.index()
__float__()   - преобразование в дробный тип
__complex__() - преобразование в комплексное число
__str__()     - преобразование в строковой тип => str(obj) or print(obj)
__bytes__()   - преобразование в байтовую строку

4. Перегрузка операторов
"""


class Vector(object):
    __slots__ = ('x', 'y')

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'{self.__class__.__name__}({self.x}, {self.y})'

    def __add__(self, other):
        """Перегрузка оператора +"""
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """Перегрузка оператора -"""
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        """Перегрузка оператора *"""
        return Vector(self.x * other.x, self.y * other.y)

    @property
    def length(self):
        return (self.x ** 2 + self.y ** 2) ** .5

    def __gt__(self, other):
        """Перегрузка оператора >"""
        return self.length > other.length

    def __eq__(self, other):
        """Перегрузка оператора =="""
        return self.length == other.length

    def __ge__(self, other):
        """Перегрузка оператора >="""
        return self.length >= other.length

    # @length.setter
    # def length(self, value):
    #     print(value)
    #
    # @length.deleter
    # def length(self):
    #     pass


v1 = Vector(-3, 4)
v2 = Vector(-3, 6)
print('Сумма векторов: v1 + v2 =', v1 + v2)
print('Разность векторов: v1 - v2 =', v1 - v2)
print('Произведение векторов: v1 * v2 =', v1 * v2)
print('Длинна вектора v1=', v1.length)
print(v1 > v2, v1 < v2, v1 == v2)
#
# v1.length = 666
# print('Длинна вектора v1=', v1.length)
# del v1.length